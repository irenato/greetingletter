<?php
/**
 * Template Name: New Home Page Design
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>



</div>

<!-- HTML START -->

<div id="sidebar-menu-wrapper" style="display:none;">
    <div id="sidebar-menu">
        <div id="sidebar-menu-inner">
<div id="logo-orange" style="margin-left:1px;margin-top:30px;">

<img id="logo-orange-img" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-white-transparent.png" style="margin-top: 18px;margin-bottom:50px; width:136px;height:73px;opacity:1;background:#eb9962;"/>

<p>Description Title
<br>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
</p>
<p>Search by name
<br>
<input id="search-enter" type="text" style="color:white;font-family: lato;"/>
</p>
<p style="width:100%;">
Search by tags
<br>

</p>
<p>


<?php wp_tag_cloud('number=50&format=list&order=RAND'); ?>



</p>
</div>

        </div>
    </div>
    <div id="sidebar-arrow">
        <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/arrow-right-2.png" />
        <div id="sidebar-ripple-wrapper">
            <div class="sidebar-ripple"></div>
            <div class="sidebar-ripple"></div>
            <div class="sidebar-ripple"></div>
            <div class="sidebar-ripple"></div>
        </div>
    </div>
</div>

<?php /*
<div id="logo-orange" style="margin-left: 20px;">

<img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/logo-white-transparent.png" style="margin-top: 80px;margin-bottom:50px; width:136px;height:73px;opacity:1;background:#eb9962;"/>

<p>Description Title
<br>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
</p>
<p>Search by name
<br>
<input id="search-enter" type="text" style="color:white;font-family: lato;"/>
</p>
<p style="width:100%;">
Search by tags
<br>
</p>
<p>


<?php wp_tag_cloud('number=50&format=list&order=RAND'); ?>



</p>
</div>
*/ ?>

<div style="width:100%;height:0px;position:absolute;z-index:100;">

<div style="width:50%;float:left;padding:20px;">
<img id="logo_white" src="http://greetingletter.co.uk/wp-content/uploads/2016/08/logo-white.png" style="margin-left: 0px;"/>
    </div>
    <div style="width:50%;float:left;padding:20px;">
<ul id="topmenu" class="start-menu" style="display:none;">
    <li style="background: url(http://greetingletter.co.uk/wp-content/uploads/2016/08/facebook.png);width:38px;height:38px;"></li>
    <li><a href="http://greetingletter.co.uk/contact/" style="color:white;">Contact</a></li>
    <li><a href="#" style="color:white;">Home</a></li>
</ul>
     </div>


</div>

<div id="hexagon-wrapper" style="padding-top:140px;width:90%;margin:0 auto;visibility:hidden;">    
    <?php
$args = array(
   
   'posts_per_page' => 52,
   'orderby' => 'rand',
   
);
        query_posts($args);
       if(have_posts()):while(have_posts()):the_post(); 
    ?>
       
<div class="hexagon-new">
<h6 style="position: absolute;
    width: 350px;
    height: 25px;
    overflow: hidden !important;
    margin-top: -40px;
    background: transparent;font-family: lato;text-shadow:none;"><?php the_title();?></h6>  
<div class="hexagon-new-inside" style="width:401px;background:gray;height:55px;position:absolute;background:url(http://greetingletter.co.uk/wp-content/uploads/2016/08/select-n-like-text.png);
    -webkit-clip-path: polygon(86% 0, 100% 100%, 100% 100%, 100% 100%, 0 100%, 0 0);margin-left:-1px;">

<div class="select-card" image="<?php echo $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>"  alt="<?php the_title();?>" style="width:50%;float:left;height: 56px;">

</div>
<div style="width:50%;float:right;">
<?php
echo do_shortcode( '[zilla_likes]' );
?>
</div>

</div>
<img src="<?php echo $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" alt="<?php the_title();?>" width="400" height="288">
</div>
    
    <?php 
            endwhile; endif; 
    ?>  
</div> <!--closing of 100px padding -->
<!---

<div class="top" style="display:none;"> </div>
<div class="bottom" style="display:none;"> </div>




<div class="next" style="display:none;"> </div>
<div class="prev" style="display:none;"> </div>

<div id="imagehovercontainer" style="display:none;position:fixed;top:30%;left:30%;border: 4px gray;">hover image appear here</div>
-->

<?php /*

<div class="rightside" style="font-family: lato;width:10px;">
<div id="logo-orange" style="display:none; margin-left: 20px;">

<img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/logo-white-transparent.png" style="margin-top: 80px;margin-bottom:50px; width:136px;height:73px;opacity:1;background:#eb9962;"/>

<p>Description Title
<br>
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
</p>
<p>Search by name
<br>
<input id="search-enter" type="text" style="color:white;font-family: lato;"/>
</p>
<p style="width:100%;">
Search by tags
<br>
<!--
<input type="button" value="Male" style="background: #e89063;
    border-radius: 5px;
    color: white;
    border: 0;
    padding: 5px 15px;float:left;margin-right:5px;margin-top:5px;"/>
<input type="button" value="Female" style="background: #e89063;
    border-radius: 5px;
    color: white;
    border: 0;
    padding: 5px 15px;float:left;margin-right:5px;margin-top:5px;"/>
<input type="button" value="Kiss" style="background: #e89063;
    border-radius: 5px;
    color: white;
    border: 0;
    padding: 5px 15px;float:left;margin-right:5px;margin-top:5px;"/>
<input type="button" value="Birthday-gift" style="background: #e89063;
    border-radius: 5px;
    color: white;
    border: 0;
    padding: 5px 15px;float:left;margin-right:5px;margin-top:5px;"/>
-->
</p>
<p>


<?php wp_tag_cloud('number=50&format=list&order=RAND'); ?>
<!--
<h3>Most common tags:</h3>
<?php wp_tag_cloud('number=50&format=list&orderby=count'); ?>

<h3>All <?php if (function_exists('tagstats')) { tagstats(); } ?> tags in alphabetical order:</h3>
<?php wp_tag_cloud('smallest=10&largest=10&number=0&format=list'); ?>
-->


</p>
</div>
<img id="left-arrow" src="http://greetingletter.co.uk/wp-content/uploads/2016/08/arrow-right-2.png"/>
</div>

*/ ?>


<div id="startscreen" style="width:100%;height:1040px;position:fixed;margin-top: -175px;z-index:10000;padding-top: 50px;-display:none;font-family: lato;">

<div style="width:100%;height:0px;position:absolute;z-index:100;">
<div style="width:50%;float:left;padding:20px;">
<img id="logo_white" src="http://greetingletter.co.uk/wp-content/uploads/2016/08/logo-white.png" style="margin-left: 0px;"/>
    </div>
    <div style="width:50%;float:left;padding:20px;">
<ul id="topmenu" style="display:none;" class="start-menu">
    <li style="background: url(http://greetingletter.co.uk/wp-content/uploads/2016/08/facebook.png);width:38px;height:38px;"></li>
    <li><a href="http://greetingletter.co.uk/contact/" style="color:white;">Contact</a></li>
    <li><a href="#" style="color:white;">Home</a></li>
</ul>
     </div>


    
</div>  
    
    <div style="    text-align: center;
    top: 40%;
    position: absolute;
    width: 733px;
    margin: 0 auto;
    left: 28%;
    color: white;margin-top: -60px;">
        <div style="font-size:27px;margin-bottom: 25px;">Welcome!</div>
        <div style="">Choose a greeting letter, personalise it and order in a few easy steps.</div>
 <!--        <div style="position:relative;width:210px;margin:0 auto;text-align:center;">
            <div style="margin-top: 30px;margin-bottom: 30px;font-weight:bold;position:absolute;top:0;left:0;" id="drag-container">
                <img id="start-heres" src="http://greetingletter.co.uk/wp-content/uploads/2016/08/start-image.png" class="ui-draggable" style="cursor:pointer;display:inline-block;"/>
                <span class="dots" style="display:inline-block;">.........................</span>
                <img id="end-heres" src="http://greetingletter.co.uk/wp-content/uploads/2016/08/end-image.png" style="cursor:pointer;position:relative;"/>
            </div>
        </div> -->

            <div class="unlock-container">
                <div class="unlock">
                    <div class="content">
                        <div class="drag">
                            <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
<!--                                 <path fill-rule="evenodd" d="M 3.51 7.67 C 3.32 7.67 3.13 7.59 2.98 7.44 L -0.78 3.63 C -1.07 3.34 -1.07 2.85 -0.78 2.56 C -0.49 2.26 -0.01 2.26 0.28 2.56 L 3.51 5.83 L 6.74 2.56 C 7.04 2.26 7.51 2.26 7.81 2.56 C 8.1 2.85 8.1 3.34 7.81 3.63 L 4.05 7.44 C 3.9 7.59 3.71 7.67 3.51 7.67" /> -->
  <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABCFBMVEUAAAD///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AAACQWfHNAAAAVnRSTlMAAjd4rNLt+ByH4hOV+cmMVC0IVuunPFcEl7AuCrP+bWyxLJjkIv0rWWoVrh76pc4p4/Qq8yN5KOzq0/YUMfwgBxnvizjIa2lYWpkG3+G2tAWvOxKI7sOwB2oAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAABtklEQVQ4y41V51rCQBA8QmgJoUkxNCGEogKKKAZExI5ioci8/6MICHK5FDJ/7na+yZW93QkhNFycm/d4fT6vh3dzLmIFf0AABSHgN5WJQQkIhSNRMRYTo5FwCJCColF3EAcSSZpJJoB4ipEdykA6w36cSQPyIc1kc8gfmZ2nkEcuS61XhKCYX1ARUNytWYJatkpFWUVpO0+hUrXMGalWUNvkJY4CscExvCfriYw0scXp3+b1BhR7YRWN+nIIoKmjz4RzVtlEYFkHLejegwAXrDKJlotwCOnZ9iUurhhlCBzpIMyw15fQ6noqDDfhESFGZVevjIAnPUQN17y+YZRR9IiGvjEhtwN076i4D41IGJqk7l7FAxUOITkXOt7a8WUcp6eDR2cJf8KznrR6QsdFsSyzhI41KbPEqsyIX4KhofVQIK3NJbivFdKQ16Oo4cVO9wptY0Ej23ZtD/C2ne8xgPf/YFzEh8Wa1U8Ux7sw+4WK6TmPK/jO0sSkBEwN7a1MgdKEIUcrI53RzGxlpDXjLn15Y83z8Xi+sWbZpFaX8P+otNmrP+Zmv4KL6/CeRaOx8PAd5vfxCwtbVOKLpwmDAAAAAElFTkSuQmCC" width="40" height="40"/>
                            </svg>
                        </div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="drop">
                            <div class="wave"></div>
                        </div>
                    </div>
                </div>
                <div>drag to start</div>
            </div>


<style>
.unlock-container{
    position: relative;
    height: 190px;
}
.unlock .content
{
    width: 186px;
    height: 40px;
    margin: auto;
    margin-top: 26px;
    position: relative;
    text-align: center;
}
.unlock .content .drag
{
    cursor:pointer;
    cursor:-webkit-grab;
    cursor:-moz-grab;
    cursor:grab;
    width: 40px;
    height: 40px;
    background-color: rgba(255,255,255,0.1);
    border: 1px solid #FFF;
    border-radius: 100px;
    position: absolute;
    z-index: 1;
    left: 0px;

    -webkit-transition: background-color .25s ease;
       -moz-transition: background-color .25s ease;
        -ms-transition: background-color .25s ease;
         -o-transition: background-color .25s ease;
            transition: background-color .25s ease;
}
.unlock .content .drag:active
{
    cursor:ns-resize;
    cursor:-webkit-grabbing;
    cursor:-moz-grabbing;
    cursor:grabbing;

    background-color: rgba(255,255,255,1);
}
.unlock .content .drag svg
{
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
    -o-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
    height: 40px;
    fill: #FFF;

    -webkit-transition: all .25s ease;
       -moz-transition: all .25s ease;
        -ms-transition: all .25s ease;
         -o-transition: all .25s ease;
            transition: all .25s ease;
}
.unlock .content .drag:active svg
{
    fill: #FFF;
}

.unlock .content .dot
{
    display: inline-block;
    position: relative;
    vertical-align: top;
    width: 2px;
    height: 100%;
}
.unlock .content .dot:after
{
    content: '';
    height: 1px;
    width: 1px;
    border-radius: 2px;
    background-color: #FFF;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
    -o-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}

.unlock .content .drop
{
    width: 40px;
    height: 40px;
    border: 2px solid rgba(255,255,255,1);
    border-radius: 100px;
    position: absolute;
    right: 0px;
    top: 0px;

    -webkit-transition: all .25s ease;
       -moz-transition: all .25s ease;
        -ms-transition: all .25s ease;
         -o-transition: all .25s ease;
            transition: all .25s ease;
}
.unlock .content .drop.active
{
    border: 1px solid rgba(255, 255, 255, 0.5);
}
.unlock .content .drop.hover
{
    border: 1px solid rgba(255, 255, 255, 0.5);
}
.unlock .content .drop .wave
{
    width: 19px;
    height: 19px;
    background-color: rgba(255,255,255, 0.2);
    border-radius: 10px;

    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
    -o-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);

    -webkit-transition: all .25s ease;
       -moz-transition: all .25s ease;
        -ms-transition: all .25s ease;
         -o-transition: all .25s ease;
            transition: all .25s ease;
}
.unlock .content .drop.active .wave
{
    background-color: rgba(255, 255, 255, 0.5);
}
.unlock .content .drop.hover .wave
{
    background-color: rgba(255, 255, 255, 0.5);
    -webkit-transform: translate(-50%,-50%) scale(1.7);
    -ms-transform: translate(-50%,-50%) scale(1.7);
    -o-transform: translate(-50%,-50%) scale(1.7);
    transform: translate(-50%,-50%) scale(1.7);
}
</style>



        <input id="start-heres" type="button" value="Start Here" style="    border: 0px;
    background: white;
    color: gray;
    padding: 10px 35px;
    border-radius: 10px;
    margin-top: 30px;visibility:hidden;"/>
    </div>  


    
<div style="
    text-align: center;
    bottom: 160px;
    position: absolute;
    width: 733px;
    margin: 0 auto;
    left: 28%;
    color: white;
    font-family: lato;
    line-height: 23px;">By using our website you agree to be legally bound by these terms, which shall take effect immediately on your first use of our website. View Terms and Conditions</div>    
</div>

<div id="fullscreen-popup" style="display:none;">
    <div id="close-popup"></div>

<div id="step0" style="width:1350px;margin:0 auto;margin-top: 40px;color: #141313 !important">

<img id="close-active" src="http://greetingletter.co.uk/wp-content/uploads/2016/08/close-active.png" style="position:absolute;margin-top: -20px;margin-left: 990px;" width="169px" />
        <div id="imagecontainer-new"></div>

<div id="proceedform">
<form id="proceed-form" action="http://greetingletter.co.uk/customize-greeting-letter/?selected-card=" method="post">
</div>
<div id="imagevaluediv">
<input name="imagesrcs" type="hidden" value=""/>
</div>
<input id="proceed-new" type="button" style="text-align:center;background:url(http://greetingletter.co.uk/wp-content/uploads/2016/08/proceed.png); width:268px;height:73px;border:0px;float: right;margin-right: 240px;margin-top: 30px;"/>
</form>



</div>


<div id="step1" style="width:1350px;margin:0 auto;margin-top: 40px;color: #141313 !important;display:none;">
<div style="width:100%;height:0px;">
<img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/logo-white.png" style="margin-left: 50px;"/>
<img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/home.png" style="float:right;margin-right: 50px;"/>

</div>
<h3 style="text-align:center;">Choose Print Size</h3>
        <div id="imagecontainer1">l</div><div id="imagecontainer2">r</div>




</div>

<div id="step2" style="width:1000px;margin:0 auto;margin-top: 40px;color: #141313 !important;display:none;">
<h1>Choose text</h1>
<div id="hexagon-text">
<input id="greettext" type="textarea" style="    width: 500px;
    height: 400px;
    margin-top: 39px;
    margin-bottom: 20px;
    float: left;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    margin-left: 79px;
    background: white;"/>
</div>
<style>
#hexagon-text {
    width: 650px;
    height: 468px;
    background: white !important;
    float: left;
    position: absolute;
    
    -webkit-clip-path: polygon(86% 0, 100% 20%, 100% 100%, 14% 100%, 0 80%, 0 0);
    clip-path: polygon(86% 0, 100% 20%, 100% 100%, 14% 100%, 0 80%, 0 0);
}
</style>        




<input id="gostep3" type="submit" value="Go to Step 3" style="margin: 0 auto;width: 150px;    margin-top: 60px;
    margin-bottom: 20px;
    float: right;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    background: white;"/>

</div>


<div id="step3" style="width:1050px;margin:0 auto;margin-top: 40px;color: #141313 !important;display:none;">

<div style="width:750px;height:520px; float:left;overflow:scroll-y;">
<h1>Back Side</h1>
<img src="http://greetingletter.co.uk/wp-content/uploads/2016/07/back-design.png" width="700px"/>       
</div>

<div style="width:260px;height:520px; float:left;overflow:scroll-y;">
<h1>Patterns</h1>
<br>
<?php query_posts('post_type=greeting_patterns&post_status=publish&posts_per_page=100&paged='. get_query_var('paged')); ?>
                    <?php if( have_posts() ): ?>
                    
                   
                        <?php while( have_posts() ): the_post(); 
                        $pattern_image= get_field('pattern_image');
?>
                            

<img src="<?php echo $pattern_image; ?>" width="50px" height="50px" style="border:2px white;    margin-right: 10px;"/>
                        <?php endwhile; ?>
                   
                    <?php endif; wp_reset_query(); ?>

</div>


<input id="gostep4" type="submit" value="Go to Step 4" style="margin: 0 auto;width: 150px;    margin-top: 60px;
    margin-bottom: 20px;
    float: right;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    background: white;"/>



</div>

<div id="step4" style="width:1000px;margin:0 auto;margin-top: 40px;color: #141313 !important;display:none;">
<h1>Order Print</h1>
        
<h2 id="greet-display"></h2>
<div id="printcontainer"></div>

<input id="gostep5" type="submit" value="Order Print" style="margin: 0 auto;width: 150px;    margin-top: 60px;
    margin-bottom: 200px;
    float: right;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    background: white;"/>

</div>


</div>
            

<script>


    $(document).ready(function() {

   $("#search-enter").on( "keyup", function(event) {
         var searchkeyword = $(this).val();
      if(event.which == 13) 
         window.location.href = 'http://greetingletter.co.uk/greeting-letter-search/?keyword='+searchkeyword;
     
    });

$('body').css('overflow','hidden');

$('.start-menu').show();

// $('#start-heres').mousemove(function(e){
//  if(e.which==1)
//  {

// $('body').css('cursor', 'wait');
//       $(this).css('cursor', 'wait');
// $('body').css('cursor','url(http://greetingletter.co.uk/wp-content/uploads/2016/08/start-image.png)');
// $(this).css('cursor','url(http://greetingletter.co.uk/wp-content/uploads/2016/08/start-image.png)');

// $('#start-heres').css('cursor','url(http://greetingletter.co.uk/wp-content/uploads/2016/08/start-image.png)');



//   $("#startscreen").hide();
//  $('body').css('overflow','scroll');

// $('#hexagon-wrapper').css('visibility','visible');
   
   

//  }
// });


/*
        
        var tid = setTimeout(mycode, 3000);
function mycode() {
    
    RandomColor = function() {
    colors = ['#368aea', '#f3af60', '#01c572', '#bc364c']
    return colors[Math.floor(Math.random()*colors.length)];
}
    RandomColor2 = function() {
    colors = ['#ba3c4a', '#fa6930', '#2e83ed', '#f3ae60']
    return colors[Math.floor(Math.random()*colors.length)];
}
    a = RandomColor(); 
    b = RandomColor2(); 
    
   $('#startscreen').css('background-color',a);
                        $('#startscreen').css('background','-moz-linear-gradient(center top, '+a+' 0%,'+b+' 100%)');
                        $('#startscreen').css('background','-webkit-gradient(linear, left top, left bottom, color-stop(0, '+a+'),color-stop(1, '+b+'))');
                        $('#startscreen').css('background-attachment','fixed');
  tid = setTimeout(mycode, 3000); // repeat myself
}
        
*/      
    
            
        var tid = setTimeout(mycode2, 10000);
function mycode2() {
    
    RandomColorb = function() {
    colors = ['#368aea', '#f3af60', '#01c572', '#bc364c']
    return colors[Math.floor(Math.random()*colors.length)];

}
    RandomColorb2 = function() {
    colors = ['#ba3c4a', '#fa6930', '#2e83ed', '#f3ae60']
    return colors[Math.floor(Math.random()*colors.length)];
 
}
    a = RandomColorb(); 
    b = RandomColorb2(); 



$('.start-menu').show();

    
    RandomBg= function() {
    colors = ['http://greetingletter.co.uk/wp-content/uploads/2016/08/bg1-1.jpg', 'http://greetingletter.co.uk/wp-content/uploads/2016/08/bg2-1.jpg', 'http://greetingletter.co.uk/wp-content/uploads/2016/08/bg3-1.jpg', 'http://greetingletter.co.uk/wp-content/uploads/2016/08/bg4-1.jpg']
    bgcolor = colors[Math.floor(Math.random()*colors.length)];
 $('#logo-orange img').css('background','url('+bgcolor +') repeat-x');
$('#logo-orange p input').css('background','url('+bgcolor +') repeat-x');
$('.wp-tag-cloud li').css('background','url('+bgcolor +') repeat-x');
return bgcolor;
}

bg = RandomBg(); 

    
   $('body').css('background-color',a);
                        $('body').css('background','-moz-linear-gradient(center top, '+a+' 0%,'+b+' 100%)');
                        $('body').css('background','-webkit-gradient(linear, left top, left bottom, color-stop(0, '+a+'),color-stop(1, '+b+'))');
                        $('body').css('background-attachment','fixed');
                        $('body').css('background','url('+bg+') repeat-x');
                        $('body').css('background-size','120%');
                        $('body').css('background-attachment','fixed');

  tid = setTimeout(mycode2, 100000); // repeat myself
}

var timeoutId;
$("#hexagon-new img").hover(function() {
var imgattrhover = $(this).attr('src');
    if (!timeoutId) {
        timeoutId = window.setTimeout(function() {
            timeoutId = null; // EDIT: added this line
            
    
    $(this).css("width", "500px");
    
    
    $('#imagehovercontainer').show();
    $('#imagehovercontainer').html('<img src="'+imgattrhover+'" width="800px" height="576px"/>');
       }, 1000);
    }
},
function () {
    if (timeoutId) {
        window.clearTimeout(timeoutId);
        timeoutId = null;
    }
    else {
       
    $('#imagehovercontainer').hide();
    }
});
/*:nth-child(5)*/
        $(".hexagon-new img").click(function(){
            $('#fullscreen-popup').show();
            $('#close-popup').css("visibility", "hidden");


            var imgattr = $(this).attr('src');
var imgalt = $(this).attr('alt');


$('#imagecontainer-new').html('<h6 style="margin-left: 66px;margin-top: 0px;">'+imgalt+'</h6><img id="sra4" src="'+imgattr+'" width="700px" style="margin-top: 0px; margin-left: 65px;"/>');
            
$('#imagevaluediv').html('<input name="imagesrcs" type="hidden" value="'+imgattr+'"/>');

$('#proceedform').html('<form action="http://greetingletter.co.uk/customize-greeting-letter/?selected-card='+imgattr+'" method="post">');


        
         });


        $(".select-card").click(function(){
            $('#fullscreen-popup').show();
            $('#close-popup').css("visibility", "hidden");


            var imgattr = $(this).attr('image');
var imgalt = $(this).attr('alt');


$('#imagecontainer-new').html('<h6 style="margin-left: 66px;margin-top: 0px;">'+imgalt+'</h6><img id="sra4" src="'+imgattr+'" width="700px" style="margin-top: 0px; margin-left: 65px;"/>');
            
$('#imagevaluediv').html('<input name="imagesrcs" type="hidden" value="'+imgattr+'"/>');

$('#proceedform').html('<form action="http://greetingletter.co.uk/customize-greeting-letter/?selected-card='+imgattr+'" method="post">');


        
         });

        $("#proceed").click(function(){
            $('#close-popup').css("visibility", "visible");

            $("#step0").hide();
            $("#step1").show();
            $("#step2").hide();
            $("#step3").hide();
            $("#step4").hide();
            //$("#gostep2").hide();

            var imgattr= $("#imagecontainer-new img").attr('src');

                    $('#fullscreen-popup').css('background-color','#3086f1');
                        $('#fullscreen-popup').css('background','-moz-linear-gradient(center top, #00c276 0%,#3086f1 100%)');
                        $('#fullscreen-popup').css('background','-webkit-gradient(linear, left top, left bottom, color-stop(0, #00c276),color-stop(1, #3086f1))');
                        $('#fullscreen-popup').css('background-attachment','fixed');

$('#imagecontainer1').html('<div style="height:500px;"><h5>Customary Size</h5><img id="sra4" src="'+imgattr+'" width="400px"/></div><input id="choose-cu" type="submit" value="Choose Customary" style="margin: 0 auto;float:left; margin-left: 250px;border: 0;padding: 15px;border-radius: 15px; background: white;font-weight:bold;"/><h7 style="text-align:center;color:white;margin-left: 10px;margin-top: 15px;float: left;">&pound;4.99</h7>');



            $('#imagecontainer2').html('<div style="height:500px;"><h5>Exceptional Size</h5><img id="sra3" src="'+imgattr+'" width="450px"/></div><input id="choose-ex" type="submit" value="Choose Exceptional" style="margin: 0 auto;float: left;  margin-left: 140px;border: 0;padding: 15px;border-radius: 15px; background: white;font-weight:bold;"/><h7 style="text-align:center;color:white;margin-left: 10px;margin-top: 15px;float: left;">&pound;6.99</h7>');
            
         });

        $("#close-active").on( "click", function() {
            $("#step0").show();
            $("#step1").hide();
            $("#step2").hide();
            $("#step3").hide();
            $("#step4").hide();
            $("#gostep2").hide();

            $('#fullscreen-popup').hide();
            
         });

        $("#proceed-new").click(function(){
            
            $("#proceed-form").submit();
var imgattr = $('#sra4').attr('src');
var imgalt = $('#sra4').attr('alt');
            window.location.href = 'http://greetingletter.co.uk/customize-greeting-letter/?selected-card='+imgattr;
            
         });
        
        
$("#logo_white").click(function(){
            
            $("#startscreen").show();
            
         });
$("#start-here").click(function(){
            
            $("#startscreen").hide();
    $('body').css('overflow','scroll');
            
         });        
        
        

        $("#close-popup").click(function(){
            $("#step0").show();

            $("#step1").hide();
            $("#step2").hide();
            $("#step3").hide();
            $("#step4").hide();
            $("#gostep2").hide();

$('#fullscreen-popup').css('background','rgba(29, 27, 27, 0.9)');

            $('#fullscreen-popup').hide();

            
         });

        $('.rightside').on('mouseenter', rscroll);

$(".rightside").mouseenter(function(){
    $(".rightside").css("width", "300px");
    $("#left-arrow").css("margin-left", "300px");
    $("#logo-orange").css("display", "block");
    $("#logo_white").css("display", "none");


    //$(".next").css("right", "300px");
    //$('body').stop();
});
$(".rightside").mouseleave(function(){
    $(".rightside").css("width", "10px");
    $("#left-arrow").css("margin-left", "0px");
    $("#logo-orange").css("display", "none");
    $("#logo_white").css("display", "block");

    //$(".next").css("right", "30px");
});
        $('.prev').show();
        $('.next').show();
        $('.top').show();
        $('.bottom').show();

        
                $('.next').on('mouseenter', rscroll);
                $('.prev').on('mouseenter', lscroll);
                $('.top').on('mouseenter', tscroll);
                $('.bottom').on('mouseenter', bscroll);
                $('.next,.prev,.top,.bottom').on('mouseleave', function() {
                    $('body').stop();
                });
                
                function rscroll() {
                    
                    $('body').animate({
                        scrollLeft: '+=25'
                    }, 100, rscroll);
                }
                
                function lscroll() {
                    
                    $('body').animate({
                        scrollLeft: '-=25'
                    }, 100, lscroll);
                }
        
                function tscroll() {
                    
                    $('body').animate({
                        scrollTop: '-=25'
                    }, 100, tscroll);
                }
                function bscroll() {
                    
                    $('body').animate({
                        scrollTop: '+=25'
                    }, 100, bscroll);
                }



$(window).scroll(function() {
    var height = $(window).scrollTop();
    var width= $(window).scrollLeft();
    var offTop = 0;
/*
    if(height  > 2190) {
    $('body').stop();
    $('.bottom').hide();
    $('body').animate({
                        scrollTop: '-=25'
                    });
    
    
    }
    if(width > 2470) {
    $('body').stop();
    $('.next').hide();
    $('body').animate({
                        scrollLeft: '-=25'
                    });
    
    
    }
    if(height  < 2190) {
    $('.bottom').show();
    }
    if(width < 2470) {
    $('.next').show();
    }


    if(height  < 10) {
    $('body').stop();
    $('.top').hide();
    $('body').animate({
                        scrollTop: '+=25'
                    });
    }
    if(width < 10) {
    $('body').stop();
    $('.prev').hide();
    $('body').animate({
                        scrollLeft: '+=25'
                    });
    }
    if(height  > 10) {
    $('.top').show();
    }
    if(width > 10) {
    $('.prev').show();
    }
*/
});

        $("#gostep2").click(function(){
                $("#step1").hide();
            $("#step2").show();
            $("#step3").hide();
            $("#step4").hide();

                     




         });
        $("#gostep3").click(function(){
                $("#step1").hide();
            $("#step2").show();
            $("#step3").hide();
            $("#step4").hide();
            

            

            
         });
        $("#gostep4").click(function(){
                $("#step1").hide();
            $("#step2").hide();
            $("#step3").show();
            $("#step4").hide();
            var greettext = $("#greettext").val();
            var imagetoprint= $("#imagecontainer2 img:nth-child(2)").attr('src');


$('#printcontainer').html('<div style="width:1000px;"><img id="sra-print" src="'+imagetoprint+'" /></div>');
            $("#greet-display").text(greettext);

         });



        $("#imagecontainer1").click(function(){
                $("#tick1").show();
                $("#tick2").hide();
            $("#gostep2").show();
         });
        $("#imagecontainer2").click(function(){
                $("#tick1").hide();
                $("#tick2").show();
            $("#gostep2").show();
         });
        
//      $('#example').sliderbutton({
//   activate: function() {
//     $('#log').val($('#log').val()+"Activate!\n");
//   }
// });

        
                        
    });
</script>

<style>


#start-heres{
 cursor:pointer;
}

#start-heres.mouseDown:hover{
 cursor:auto; /* or whatever cursor */
}

body {
    width: 100% !important;
    max-width: 100%!important;
    -height: 2880px !important;
    -max-height: 2880px !important;
    -overflow:hidden;
/*
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-user-drag: none;
      user-drag: none;
      -webkit-touch-callout: none;

*/

/*  
background-image:url(http://greetingletter.co.uk/wp-content/uploads/2016/08/orange.jpg);
background-size:100%;
    background-position-y: -35px;
*/
    background-color: #e37b5e;
    background: -moz-linear-gradient(center top, #f3b060 0%,#e37b5e 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #f3b060),color-stop(1, #e37b5e));
    background-attachment: fixed;

}
/*  
#startscreen{
        background-color: #e37b5e;
    -background-image:url('http://greetingletter.co.uk/wp-content/uploads/2016/08/Website-Color-03.png');
background-image:url('<?php echo get_stylesheet_directory_uri(); ?>/images/startscreen-nav-bg-last.png');
startscreen-nav-bg-last.png
    background: -moz-linear-gradient(center top, #f3b060 0%,#e37b5e 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #f3b060),color-stop(1, #e37b5e));
    background-attachment: fixed;

}
*/
@font-face {
    font-family: lato-custom;
    src: url(sansation_light.woff);
}

.hexagon-new:hover {
    cursor:pointer;
}

    #topmenu li{
        float:right;
        margin-right:30px;
        color:white;
        font-size: 20px;
    }
.wp-tag-cloud li 
{
border-radius: 5px;
    color: white;
    border: 0px;
    padding: 5px 15px;
    float: left;
    margin-right: 5px;
    margin-top: 5px;
    background:orange;
    font-size: 18px;
}
.wp-tag-cloud li a
{
    font-size: 18px !important;
    color: white;
}
.wp-tag-cloud{
    margin-left: 0px;
}
</style>

<script>

    jQuery(document).ready(function(){

        function hideStartScreen(){
            jQuery("#startscreen").hide();
            jQuery('body').css('overflow','scroll');
            jQuery('#hexagon-wrapper').css('visibility','visible');
        }

        jQuery('#start-heres').draggable({
            axis: 'x',
            containment: '#drag-container',
            drag: function(){
                var $start = jQuery('#start-heres');
                var $end = jQuery('#end-heres');
                if(parseInt($start.css('left').replace('px')) < 0){
                    $start.css('left', 0);
                }
                if(Math.round(Math.abs($start.position().left - $end.position().left)) <= 10){
                    hideStartScreen();
                }
            }
        });

        // (function() {

        //     var bodyEl = document.body,
        //         content = document.querySelector( '.sidebar-content-wrap' ),
        //         openbtn = document.getElementById( 'sidebar-open-button' ),
        //         closebtn = document.getElementById( 'sidebar-close-button' ),
        //         arrow = document.getElementById('sidebar-arrow'),
        //         isOpen = false,

        //         morphEl = document.getElementById( 'sidebar-morph-shape' ),
        //         s = Snap( morphEl.querySelector( 'svg' ) );
        //         path = s.select( 'path' );
        //         initialPath = this.path.attr('d'),
        //         pathOpen = morphEl.getAttribute( 'data-morph-open' ),
        //         steps = morphEl.getAttribute( 'data-morph-open' ).split(';');
        //         stepsTotal = steps.length;
        //         isAnimating = false;

        //     $(document).on('mouseenter mouseleave', '.sidebar-menu-wrap', function(){
        //         toggleMenu();
        //     });

        //     // setInterval(function(){
        //     //     var $el = $(':hover');
        //     //     if($el.is('.sidebar-menu-wrap *')){
        //     //         return;
        //     //     } else {
        //     //         if($('body').is('.sidebar-show-menu')){
        //     //             toggleMenu();
        //     //         }
        //     //     }
        //     // }, 1000);

        //     function toggleMenu() {
        //         if( isAnimating ) return false;
        //         isAnimating = true;
        //         if( isOpen ) {
        //             $(bodyEl).removeClass('sidebar-show-menu');
        //             // animate path
        //             setTimeout( function() {
        //                 // reset path
        //                 path.attr( 'd', initialPath );
        //                 isAnimating = false; 
        //             }, 300 );
        //         }
        //         else {
        //             $(bodyEl).addClass('sidebar-show-menu');
        //             // animate path
        //             var pos = 0,
        //                 nextStep = function( pos ) {
        //                     if( pos > stepsTotal - 1 ) {
        //                         isAnimating = false; 
        //                         return;
        //                     }
        //                     path.animate( { 'path' : pathOpen }, 400, mina.easeinout, function() { isAnimating = false; } );

        //                     pos++;
        //                 };



        //             nextStep(pos);
        //         }
        //         isOpen = !isOpen;
        //     }

        // })();

    });

    var $wrapper = $('#sidebar-menu-wrapper');
    var $menu = $('#sidebar-menu');
    var $inner = $('#sidebar-menu-inner');
    var $arrow = $('#sidebar-arrow');

    $arrow.on('click', function(e){
        $('#sidebar-ripple-wrapper').addClass('temp-hide');
        setTimeout(function(){
            $('#sidebar-ripple-wrapper').removeClass('temp-hide');
         }, 400);
        if($wrapper.is('.active')){
            $wrapper.addClass('closing');
            setTimeout(function(){
                $wrapper.removeClass('closing');
            }, 400);
        } else {
            $wrapper.addClass('opening');
            setTimeout(function(){
                $wrapper.removeClass('opening');
            }, 400);
        }
        $wrapper.toggleClass('active');
    });

    $('#sidebar-menu, #sidebar-arrow').on('mouseover', function(e){
        $wrapper.addClass('hover');
    }).on('mouseout', function(e){
        setTimeout(function(){
            if($wrapper.is('.active')) return;
            $(this).removeClass('hover active-hover-clip');
        }, 100);
    });

    $arrow.on('mouseover', function(){
        $wrapper.addClass('hover');
        if($wrapper.is('.active')){
            $wrapper.addClass('active-hover-clip');
        }
    });

    $arrow.on('mouseout', function(){
        $wrapper.removeClass('active-hover-clip');
    });

    // $(document).on('click', '*:not("#sidebar-menu, #sidebar-menu *")', function(e){
    //     hideMenu();
    // });

    $wrapper.on('click', function(e){
        e.stopPropagation();
    });

    setInterval(function(){
        if(!$(':hover').is($wrapper) && !$(':hover').is('#sidebar-menu-wrapper *') && !$wrapper.is('.active')){
            $('#sidebar-menu-wrapper').removeClass('hover active active-hover-clip');
        }
        if(!$wrapper.is('.active')){
            $('#sidebar-menu-wrapper').removeClass('active active-hover-clip');
        }
    }, 10);

</script>



<script>
        jQuery(".unlock .drag").draggable({
            axis: 'x',
            containment: 'parent',
            drag: function(event, ui) {
                jQuery('.unlock .content .drop').addClass('active');
                if (ui.position.left > 105) {
                    jQuery('.unlock .content .drop').addClass('hover');
                }
            },
            stop: function(event, ui) {
                if (ui.position.left < 105) {
                    jQuery(this).animate({
                        left: 0
                    })
                    jQuery('.unlock .content .drop').removeClass('hover');
                    jQuery('.unlock .content .drop').removeClass('active');
                } else {
                    jQuery(this).animate({
                        left: 0 
                    });
                    jQuery("#startscreen").hide();
                    jQuery('body').css('overflow','scroll');
                    jQuery('#sidebar-menu-wrapper').show();
                    jQuery('#hexagon-wrapper').css('visibility','visible');
                    jQuery('.unlock .content .drop').removeClass('hover');
                    jQuery('.unlock .content .drop').removeClass('active');
                }
            }
        });
</script>

<style>
    #sidebar-menu-wrapper{
        display: none;
    }
    #sidebar-menu{
        position: fixed;
        width: 280px;
        height: 100%;
        height: 100vh;
        background-color: white;
        top: 0;
        left: -270px;
        z-index: 1000;
        border: 1px solid transparent;
        box-sizing: border-box;
        border-radius: 0%;
        -webkit-clip-path: ellipse(100% 100% at 50% 50%);
        clip-path: ellipse(100% 100% at 50% 50%);
        -moz-clip-path: ellipse(100% 100% at 50% 50%);
        transition: -webkit-clip-path .1s ease 0s, width .1s ease .3s, left .3s ease .2s;
    }
    #sidebar-menu-wrapper.active{
        overflow-x: hidden;
    }
    #sidebar-menu-wrapper.hover #sidebar-menu,
    #sidebar-menu-wrapper.active #sidebar-menu{
        -webkit-clip-path: ellipse(50% 100% at 50% 50%);
        clip-path: ellipse(50% 100% at 50% 50%);
        -moz-clip-path: ellipse(50% 100% at 50% 50%);
        left: -260px;
        transition: -webkit-clip-path .2s ease 0s, width .1s ease .3s, left .3s ease .1s, width .2s ease, padding-left .2s ease;
    }
    #sidebar-menu-wrapper.active #sidebar-menu{
        left: -100px;
        padding-left: 100px;
        width: 420px;
        -webkit-clip-path: polygon(100% 0%, 100% 50%, 100% 52%, 100% 100%, 0 100%, 0 0);
        -moz-clip-path: polygon(100% 0%, 100% 50%, 100% 52%, 100% 100%, 0 100%, 0 0);
        clip-path: polygon(100% 0%, 100% 50%, 100% 52%, 100% 100%, 0 100%, 0 0);
    }
    #sidebar-menu-wrapper.active.active-hover-clip #sidebar-menu{
        transition: -webkit-clip-path .4s ease, clip-path .4s ease;
        -webkit-clip-path: polygon(100% 0%, 94% 50%, 94% 52%, 100% 100%, 0 100%, 0 0);
        -moz-clip-path: polygon(100% 0%, 100% 50%, 100% 52%, 100% 100%, 0 100%, 0 0);
        clip-path: polygon(100% 0%, 100% 50%, 100% 52%, 100% 100%, 0 100%, 0 0);
    }
    #sidebar-menu-wrapper #sidebar-menu-inner{
        opacity: 0;
        padding: 0 1em 0;
    }
    #sidebar-menu-wrapper.active #sidebar-menu-inner{
        opacity: 1;
    }
    #sidebar-arrow{
        position: fixed;
        top: 50%;
        top: 50vh;
        margin-top: -30px;
        left: 0;
        margin-left: 6px;
        transition: margin-left .2s ease 0s, transform .2s ease 0s;
        cursor: pointer;
        z-index: 1001;
        width: 21px;
        height: 60px;
        transform-style: preserve-3d;
        transform: scale(0.75);
        transform-style: preserve-3d;
        backface-visibility: hidden;
    }
    #sidebar-arrow::before {
        content: '';
        display: block;
        position: relative;
        background: white;
        width: 70px;
        height: 60px;
        z-index: -1;
        border-radius: 1000%;
        left: -45px;
        top: 0;
    }
    #sidebar-menu-wrapper.active #sidebar-arrow::before{
        left: -30px;
    }
    #sidebar-menu-wrapper.active #sidebar-arrow img{
        position: relative;
        top: -60px;
        left: -21px;
    }
    #sidebar-menu-wrapper.active-hover-clip #sidebar-arrow::before{
        left: -20px;
    }
    #sidebar-menu-wrapper.active-hover-clip #sidebar-arrow img{
        position: relative;
        top: -60px;
        left: -11px;
    }
    #sidebar-menu-wrapper.hover #sidebar-arrow{
        transition: margin-left .2s ease .2s, transform .2s ease;
    }
    #sidebar-menu-wrapper.hover:not(.active) #sidebar-arrow{
        transform: scale(1);
        margin-left: 16px;
    }
    #sidebar-menu-wrapper.active #sidebar-arrow{
        transition: margin-left .2s ease 0s, transform .2s ease;
        margin-left: 290px;
        transform: scale(-1);
    }
    #sidebar-menu-wrapper.active-hover-clip:not(.opening) #sidebar-arrow{
        margin-left: 276px;
    }
    #sidebar-menu-wrapper.opening #sidebar-arrow{
        transition: margin-left .45s ease .0s, transform 0s ease 0s !important;
    }
    #sidebar-menu-wrapper.opening #sidebar-arrow::before{
        transition: left .4s ease 0s;
    }
    #sidebar-menu-wrapper.closing #sidebar-arrow{
        transition: margin-left .4s ease 0s, transform 0s ease 0s !important;
    }
    #sidebar-arrow img{
        display: block;
        -webkit-clip-path: inset(20% 0 20% 0);
        clip-path: inset(20% 0 20% 0);
        -moz-clip-path: inset(20% 0 20% 0);
        position: relative;
        top: -60px;
    }
    #sidebar-ripple-wrapper{
        position: absolute;
        top: 60px;
        left: -60px;
        z-index: -1;
    }
    #sidebar-menu-wrapper.active{
        top: 0;
        left: 0;
    }
    #sidebar-menu-wrapper:not(.active) #sidebar-ripple-wrapper .sidebar-ripple,
    #sidebar-menu-wrapper:not(.hover) #sidebar-ripple-wrapper .sidebar-ripple{
        opacity: 0;
    } 
    #sidebar-ripple-wrapper .sidebar-ripple{
        /*transform: scale(0);*/
        transform-origin: 0 50%;
        transition: all 50ms linear;
        width: 100px;
        height: 76px;
        border-radius: 100%;
        position: absolute;
        top: -8px;
        left: 0;
        opacity: 0;
    }
    #sidebar-menu-wrapper.hover #sidebar-ripple-wrapper .sidebar-ripple{
        left: -80px;
        opacity: 1;
    }
    #sidebar-menu-wrapper.active #sidebar-ripple-wrapper .sidebar-ripple{
        -webkit-clip-path: polygon(50% 0, 0 0, 0 100%, 50% 100%);
        clip-path: polygon(50% 0, 0 0, 0 100%, 50% 100%);
        -moz-clip-path: polygon(50% 0, 0 0, 0 100%, 50% 100%);
        left: 24px;
        top: -90px;
        opacity: 0;
    }
    #sidebar-menu-wrapper.active-hover-clip #sidebar-ripple-wrapper .sidebar-ripple{
        opacity: 1;
    }
    #sidebar-ripple-wrapper .sidebar-ripple:nth-of-type(1){
        box-shadow: 0px 0px 0 20px rgba(255, 255, 255, .15);
        transition-delay: 50ms;
    }
    #sidebar-ripple-wrapper .sidebar-ripple:nth-of-type(2){
        box-shadow: 0px 0px 0 40px rgba(255, 255, 255, .15);
        transition-delay: 100ms;
    }
    #sidebar-ripple-wrapper .sidebar-ripple:nth-of-type(3){
        box-shadow: 0px 0px 0 60px rgba(255, 255, 255, .15);
        transition-delay: 150ms;
    }
    #sidebar-ripple-wrapper .sidebar-ripple:nth-of-type(4){
        box-shadow: 0px 0px 0 80px rgba(255, 255, 255, .15);
        transition-delay: 200ms;
    }
    #sidebar-menu-wrapper #sidebar-ripple-wrapper{
        opacity: 0;
        transition: opacity 0s ease .3s;
        transform: rotate(180deg);
    }
    #sidebar-menu-wrapper.hover #sidebar-ripple-wrapper{
        opacity: 1;
        transition-delay: 0s;
    }
    #sidebar-menu-wrapper.active:not(.opening) #sidebar-ripple-wrapper{
        top: 78px;
        left: -30px;
    }
    #sidebar-ripple-wrapper.temp-hide{
        position: fixed;
        top: -9999px;
        left: -9999px !important;
        display: none;
        opacity: 0 !important;
        transition: all 0s ease 0 !important;
    }
    #sidebar-menu-wrapper.active #sidebar-ripple-wrapper{
        transform: rotate(0deg);
    }
/*    #sidebar-menu-wrapper.active-hover-clip #sidebar-ripple-wrapper .sidebar-ripple{
        left: -29px;
        opacity: 1;
    }*/

</style>

<?php
get_footer();
