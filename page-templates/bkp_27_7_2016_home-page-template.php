<?php
/**
 * Template Name: Home Page Template bkp
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>



<!--
<div id="demo-menu" class="closed">
  <div id="min-max-tag"><i class="fa fa-chevron-circle-left"></i></div>

</div>
-->
  
    <?php
$args = array(
   
   'posts_per_page' => 143,
   'orderby' => 'rand',
   
);
	    query_posts($args);
	   if(have_posts()):while(have_posts()):the_post(); 
	?>
	   
	   
<div id="hexagon">
<img src="<?php echo $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" alt="<?php the_title();?>" width="400" height="288">
</div>
	
	<?php 
			endwhile; endif; 
	?>	

<div class="next" style="display:none;"> </div>
<div class="prev" style="display:none;"> </div>
<div class="top" style="display:none;"> </div>
<div class="bottom" style="display:none;"> </div>

<div id="imagehovercontainer" style="display:none;position:fixed;top:30%;left:30%;border: 4px gray;">hover image appear here</div>

<div class="rightside">
<div class="vertical-text">Settings</div>
1. Choose Design
2. Choose Print Size
3. Choose Text
4. Back Side
5. Order Print
</div>




<div id="fullscreen-popup" style="display:none;">
	<div id="close-popup"></div>
<div id="step1" style="width:1350px;margin:0 auto;margin-top: 40px;color: #141313 !important">
<h1>Choose Print Size</h1>
		<div id="imagecontainer1"></div><div id="imagecontainer2"></div>


<input id="gostep2" type="submit" value="Go to Step 2" style="margin: 0 auto;width: 150px;    margin-top: 60px;
    margin-bottom: 20px;
    float: right;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    background: white;
    display:none;"/>

</div>

<div id="step2" style="width:1000px;margin:0 auto;margin-top: 40px;color: #141313 !important;display:none;">
<h1>Choose text</h1>
<div id="hexagon-text">
<input id="greettext" type="textarea" style="    width: 500px;
    height: 400px;
    margin-top: 39px;
    margin-bottom: 20px;
    float: left;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    margin-left: 79px;
    background: white;"/>
</div>
<style>
#hexagon-text {
    width: 650px;
    height: 468px;
    background: white !important;
    float: left;
    position: absolute;
    /* top: 1440px; */
    /* left: 2000px; */
    -webkit-clip-path: polygon(86% 0, 100% 20%, 100% 100%, 14% 100%, 0 80%, 0 0);
    clip-path: polygon(86% 0, 100% 20%, 100% 100%, 14% 100%, 0 80%, 0 0);
}
</style>		




<input id="gostep3" type="submit" value="Go to Step 3" style="margin: 0 auto;width: 150px;    margin-top: 60px;
    margin-bottom: 20px;
    float: right;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    background: white;"/>

</div>


<div id="step3" style="width:1050px;margin:0 auto;margin-top: 40px;color: #141313 !important;display:none;">

<div style="width:750px;height:520px; float:left;overflow:scroll-y;">
<h1>Back Side</h1>
<img src="http://exinary.us/greetingletter/wp-content/uploads/2016/07/back-design.png" width="700px"/>		
</div>

<div style="width:260px;height:520px; float:left;overflow:scroll-y;">
<h1>Patterns</h1>
<br>
<?php query_posts('post_type=greeting_patterns&post_status=publish&posts_per_page=100&paged='. get_query_var('paged')); ?>
					<?php if( have_posts() ): ?>
					
                   
						<?php while( have_posts() ): the_post(); 
						$pattern_image= get_field('pattern_image');
?>
							

<img src="<?php echo $pattern_image; ?>" width="50px" height="50px" style="border:2px white;    margin-right: 10px;"/>
						<?php endwhile; ?>
                   
					<?php endif; wp_reset_query(); ?>

</div>


<input id="gostep4" type="submit" value="Go to Step 4" style="margin: 0 auto;width: 150px;    margin-top: 60px;
    margin-bottom: 20px;
    float: right;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    background: white;"/>



</div>

<div id="step4" style="width:1000px;margin:0 auto;margin-top: 40px;color: #141313 !important;display:none;">
<h1>Order Print</h1>
		
<h2 id="greet-display"></h2>
<div id="printcontainer"></div>

<input id="gostep5" type="submit" value="Order Print" style="margin: 0 auto;width: 150px;    margin-top: 60px;
    margin-bottom: 200px;
    float: right;
    margin-right: 50px;
    border: 0;
    padding: 15px;
    border-radius: 15px;
    background: white;"/>

</div>


</div>
			
<!--   
<div class="photopile-wrapper" style="padding: 50px; background-image: none; background-position: 50% center, 50% center; background-repeat: no-repeat;">
  <ul class="photopile" style="display: inline-block; opacity: 1;">
  
  <?php
	    query_posts("post_type=post&showposts=100");
	   if(have_posts()):while(have_posts()):the_post(); ?>
	   
	   
		
    <li style="transform: rotate(0deg); margin: -25px; z-index: 1; background: transparent;" class="ui-draggable first">
      <a href="<?php echo $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" style="padding: 2px;">
        <img src="<?php echo $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') ); ?>" alt="<?php the_title();?>" width="431" height="311">
      </a>
    </li>
		<?php
			endwhile; endif; ?>	
			
 
  </ul>
</div>  
   
   -->
   
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/jquery-ui.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap-select.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap-switch.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/photopile.js"></script>
	
	<!--
	<div id="photopile-active-image-container" style="display: none; position: absolute; padding: 2px; z-index: 100; width:1000px;"><img id="photopile-active-image" style="display: block;width:1000px;"><div id="photopile-active-image-info" style="opacity: 0;"><p></p></div><div id="photopile-nav-next"></div><div id="photopile-nav-prev"></div></div>
  	
	
	
	
	
<div id="demo1">
    <img alt="background" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/background.png" />
    <div id="particle-target" ></div>
	
    <img alt="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" />
	
	
	
</div>

-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.particleground.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/jquery.logosDistort.js"></script>
<script>
    var particles = {
        dotColor: '#1ec5ee',
        lineColor: '#0a4e90',
        density: 20000,
        parallax: true
    };
    var particleDensity;
    var options = {
            effectWeight: 1,
            outerBuffer: 1.05,
            elementDepth: 200,
            perspectiveMulti: 1.5,
            enableSmoothing: true,
            onInit: function() {
              $('#particle-target').particleground(particles);
            }
        };

    $('#min-max-tag').click(function(){
      if ($('#demo-menu').hasClass('closed')) {
        $('#demo-menu').removeClass('closed').addClass('open');
      } else {
        $('#demo-menu').removeClass('open').addClass('closed');
      }
    });

    $(document).ready(function() {

      //$('#demo1').logosDistort(options);
	  //alert('test');
		
//$('html, body').dragScroll({});


var timeoutId;
$("#hexagon img").hover(function() {
var imgattrhover = $(this).attr('src');
    if (!timeoutId) {
        timeoutId = window.setTimeout(function() {
            timeoutId = null; // EDIT: added this line
            //$("#SeeAllEvents").slideDown('slow');
	
	$(this).css("width", "500px");
	
	//alert(imgattrhover);
	$('#imagehovercontainer').show();
	$('#imagehovercontainer').html('<img src="'+imgattrhover+'" width="800px" height="576px"/>');
       }, 1000);
    }
},
function () {
    if (timeoutId) {
        window.clearTimeout(timeoutId);
        timeoutId = null;
    }
    else {
       //$("#SeeAllEvents").slideUp('slow');
	$('#imagehovercontainer').hide();
    }
});

		$("#hexagon img").click(function(){
        	$('#fullscreen-popup').show();
			var imgattr = $(this).attr('src');
			//alert('te');
			
			$('#imagecontainer1').html('<img id="tick1" src="http://exinary.us/greetingletter/wp-content/uploads/2016/07/tick.png" style="position:relative;display:none;float:right;    margin-right: 20px;" width="20px" height="20px"/><img id="sra4" src="'+imgattr+'" width="550px" height="396px"/><h1 style="text-align:center;">Price : $5.99</h1>');
			$('#imagecontainer2').html('<img id="tick2" src="http://exinary.us/greetingletter/wp-content/uploads/2016/07/tick.png" style="position:relative;display:none;float:right;    margin-right: 20px;" width="20px" height="20px"/><img id="sra3" src="'+imgattr+'" width="660px" height="475px"/><h1 style="text-align:center;">Price : $6.99</h1>');
			//$('#fullscreen-popup').css({'background-image': 'url(' + imgattr + ')'});
   		 });

		$("#close-popup").click(function(){
			$("#step1").show();
			$("#step2").hide();
			$("#step3").hide();
			$("#step4").hide();
			$("#gostep2").hide();

        	$('#fullscreen-popup').hide();
			//$( "#hexagon" ).focus();
   		 });
/*
		$('#hexagon img').click(function() {
    		  var index = $(this).index();
    		  console.log(index);
  		});
*/
		$('.rightside').on('mouseenter', rscroll);

$(".rightside").mouseenter(function(){
    $(".rightside").css("width", "200px");
    $(".next").css("right", "200px");
    $('body').stop();
});
$(".rightside").mouseleave(function(){
    $(".rightside").css("width", "30px");
    $(".next").css("right", "30px");
});
		$('.prev').show();
		$('.next').show();
		$('.top').show();
		$('.bottom').show();

		
				$('.next').on('mouseenter', rscroll);
                $('.prev').on('mouseenter', lscroll);
				$('.top').on('mouseenter', tscroll);
				$('.bottom').on('mouseenter', bscroll);
                $('.next,.prev,.top,.bottom').on('mouseleave', function() {
                    $('body').stop();
                });
                
                function rscroll() {
					
                    $('body').animate({
                        scrollLeft: '+=25'
                    }, 100, rscroll);
                }
                
                function lscroll() {
					
                    $('body').animate({
                        scrollLeft: '-=25'
                    }, 100, lscroll);
                }
		
                function tscroll() {
					
                    $('body').animate({
                        scrollTop: '-=25'
                    }, 100, tscroll);
                }
                function bscroll() {
					
                    $('body').animate({
                        scrollTop: '+=25'
                    }, 100, bscroll);
                }



$(window).scroll(function() {
    var height = $(window).scrollTop();
    var width= $(window).scrollLeft();
    var offTop = 0;

    if(height  > 2190) {
	$('body').stop();
	$('.bottom').hide();
	$('body').animate({
                        scrollTop: '-=25'
                    });
	
	//$('html, body').scrollTop(offTop);
    }
    if(width > 2470) {
	$('body').stop();
	$('.next').hide();
	$('body').animate({
                        scrollLeft: '-=25'
                    });
	
	//$('html, body').scrollLeft(offTop);
    }
    if(height  < 2190) {
	$('.bottom').show();
    }
    if(width < 2470) {
	$('.next').show();
    }


    if(height  < 10) {
	$('body').stop();
	$('.top').hide();
	$('body').animate({
                        scrollTop: '+=25'
                    });
    }
    if(width < 10) {
	$('body').stop();
	$('.prev').hide();
	$('body').animate({
                        scrollLeft: '+=25'
                    });
    }
    if(height  > 10) {
	$('.top').show();
    }
    if(width > 10) {
	$('.prev').show();
    }

});

		$("#gostep2").click(function(){
        		$("#step1").hide();
			$("#step2").show();
			$("#step3").hide();
			$("#step4").hide();
   		 });
		$("#gostep3").click(function(){
        		$("#step1").hide();
			$("#step2").hide();
			$("#step3").show();
			$("#step4").hide();
			

			

			
   		 });
		$("#gostep4").click(function(){
        		$("#step1").hide();
			$("#step2").hide();
			$("#step3").hide();
			$("#step4").show();
			var greettext = $("#greettext").val();
			var imagetoprint= $("#imagecontainer2 img:nth-child(2)").attr('src');
			//alert(imagetoprint);
			//alert(greettext);
/*
			$('#printcontainer').html('<div style="width:1000px;height:1409px;background:url(http://exinary.us/greetingletter/wp-content/uploads/2016/07/letter-background.jpg);"><img id="sra-print" src="'+imagetoprint+'" style="-webkit-transform: rotate(45deg);margin-top: 468px;margin-left: 163px;width: 674px;height: 486px;position:absolute;z-index:100;"/><img src="http://exinary.us/greetingletter/wp-content/uploads/2016/07/letter-background.jpg" style="position:absolute;"/></div>');
			$("#greet-display").text(greettext);
*/

$('#printcontainer').html('<div style="width:1000px;"><img id="sra-print" src="'+imagetoprint+'" /></div>');
			$("#greet-display").text(greettext);

   		 });

/*
		$("#gostep5").click(function(){

        		var divContents = $("#printcontainer").html();
            var printWindow = window.open('', '', 'height=600,width=1000');
            printWindow.document.write('<html><head><title>Greeting Letter from greetingletter.com</title>');
            printWindow.document.write('</head><body style="margin: 25mm 25mm 25mm 25mm;">');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print([1]);

   		 });
*/

		$("#imagecontainer1").click(function(){
        		$("#tick1").show();
        		$("#tick2").hide();
			$("#gostep2").show();
   		 });
		$("#imagecontainer2").click(function(){
        		$("#tick1").hide();
        		$("#tick2").show();
			$("#gostep2").show();
   		 });

		//$('html, body').animate({ scrollTop: $('#hexagon:nth-child(14)').offset().top}, 1000);
		//$('html, body').animate({ scrollLeft: $('#hexagon:nth-child(14)').offset().top}, 1000);
		//window.scroll(800, 800);
						
    });
</script>


<?php
get_footer();
