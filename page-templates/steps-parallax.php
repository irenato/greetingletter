<?php
/**
 * Template Name: Steps Parallalax
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header();


$imageselected = $_GET['selected-card'];
if ($imageselected == '') {
    echo '<meta http-equiv="refresh" content="2;url=new-design/" />';
} else {
    ?>

    </div>
    <div style="width:100%;height:0px;position:fixed;z-index:100;">
        <div style="width:300px;float:left;padding:20px;">
            <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/logo-white.png"
                 style="margin-left: 50px;"/>
        </div>
        <div style="width:400px;float:right;padding:20px;">
            <ul id="topmenu">
                <li style="background: url(http://greetingletter.co.uk/wp-content/uploads/2016/08/facebook.png);width:38px;height:38px;"></li>
                <li><a href="http://greetingletter.co.uk/contact/" style="color:white;">Contact</a></li>
                <li><a href="http://greetingletter.co.uk/new-design/" style="color:white;">Home</a></li>
            </ul>
        </div>
    </div>
    </head>

    <body>
    <div id="page-preloader"><span class="spinner"></span></div>
    <!--
<ul id="menu" style="position:fixed;bottom: 40px;left: 82%;">
	<li data-menuanchor="firstPage"><a href="#firstPage">1</a></li>
	<li data-menuanchor="secondPage"><a href="#secondPage">2</a></li>
	<li data-menuanchor="3rdPage"><a href="#3rdPage">3</a></li>
	<li data-menuanchor="4thpage"><a href="#4thpage">4</a></li>

</ul>
-->
    <div id="fullpage">
        <div class="section " id="section0">
            <div id="step1"
                 style="width:1350px;margin:0 auto;padding-top: 55px;color: #141313 !important; height: 100vh;">

                <h3 style="text-align: center;margin-bottom: 100px;    font-family: lato;
    text-shadow: none;
    font-size: 30px;">Choose Print Size</h3>
                <div id="imagecontainer1">

                    <div style="height:500px;">
                        <h5 style="margin-bottom: 30px;    font-family: lato;
    text-shadow: none;
    font-size: 25px;">Customary Size</h5>

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/small-height.png" style="    position: absolute;
    margin-left: -150px;
    margin-top: 90px;"/>

                        <img id="sra4" src="<?php echo $imageselected; ?>" width="400px"/></div>


                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/small-width.png" style="    position: absolute;
    margin-left: -60px;
    margin-top: -140px;"/>

                    <a href="#secondPage">
                        <input id="choose-cu" type="submit" value="Choose Customary"
                               style="margin: 0 auto;float:left; margin-left: 250px;border: 0;padding: 15px;border-radius: 15px; background: white;font-weight:bold;"/>
                    </a>
                    <h7 style="text-align:center;color:white;margin-left: 10px;margin-top: 15px;float: left;">&pound;4.99</h7>

                    <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/border-arrow.png" style="    top: 117px;
    position: absolute;margin-left: 18px;"/>
                    <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/scrolldown-2.png" style="    margin-top: 55px;
    position: absolute;margin-left: -15px;"/>

                </div>
                <div id="imagecontainer2">
                    <div style="height:500px;">
                        <h5 style="margin-bottom: 30px;    font-family: lato;
    text-shadow: none;
    font-size: 25px;">Exceptional Size</h5>

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/big-height.png" style="    position: absolute;
    margin-left: 470px;
    margin-top: 150px;"/>
                        <img id="sra3" src="<?php echo $imageselected; ?> " width="450px"/></div>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/big-width.png" style="    position: absolute;
    margin-left: -50px;
    margin-top: -110px;"/>
                    <a href="#secondPage">
                        <input id="choose-ex" type="submit" value="Choose Exceptional"
                               style="margin: 0 auto;float: left;  margin-left: 140px;border: 0;padding: 15px;border-radius: 15px; background: white;font-weight:bold;"/>
                    </a>
                    <h7 style="text-align:center;color:white;margin-left: 10px;margin-top: 15px;float: left;">&pound;6.99</h7>
                </div>
                <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/1by4.png"
                     style="position: absolute;left:50px;bottom:75px;"/>
            </div>
        </div>
        <div class="section" id="section1">
            <div class="intro">
                <div id="step2" style="width:auto;margin:0 auto; color: #141313 !important;display:block;">
                    <h3 style="text-align: center;    font-family: lato;
    text-shadow: none;
    font-size: 30px;">Personalize Text</h3>
                    <div>
                        <div id="textTransform">
                            <div class="textTransform">
                                <label for="font">Font Style
                                    <br>
                                    <div class="btn-group">
                                        <div class="btn"> Arial Regular</div>
                                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                            <span class="fa fa-chevron-down" title="Toggle dropdown menu"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-value="Indie Flower"> Indie Flower </a></li>
                                            <li><a href="#" data-value="Roboto Mono"> Roboto Mono </a></li>
                                            <li><a href="#" data-value="Cantarell"> Cantarell </a></li>
                                            <li><a href="#" data-value="Droid Serif"> Droid Serif </a></li>
                                            <li><a href="#" data-value="Yatra One"> Yatra One </a></li>
                                            <li><a href="#" data-value="David Libre">David Libre</a></li>
                                            <li><a href="#" data-value="Oswald">Oswald</a></li>
                                            <li><a href="#" data-value="Montserrat">Montserrat</a></li>
                                            <li><a href="#" data-value="Raleway">Raleway</a></li>
                                        </ul>
                                    </div>
                                </label>
                                <label for="fontSize">Font Size
                                    <br>
                                    <div class="btn-group">
                                        <div class="btn"> 16 pt.</div>
                                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                            <span class="fa fa-chevron-down" title="Toggle dropdown menu"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" data-value="10"> 10 pt. </a></li>
                                            <li><a href="#" data-value="12"> 12 pt. </a></li>
                                            <li><a href="#" data-value="14"> 14 pt. </a></li>
                                            <li><a href="#" data-value="16"> 16 pt. </a></li>
                                            <li><a href="#" data-value="18"> 18 pt. </a></li>
                                            <li><a href="#" data-value="20"> 20 pt. </a></li>
                                        </ul>
                                    </div>
                                </label>
                                <label for="textAlign">Text Align
                                    <br>
                                    <div class="btn-group text-align">
                                        <a class="btn btn-default" href="#" data-align="left">
                                            <i class="fa fa-align-left" title="Align Left"></i>
                                        </a>
                                        <a class="btn btn-default" href="#" data-align="center">
                                            <i class="fa fa-align-center" title="Align Center"></i>
                                        </a>
                                        <a class="btn btn-default" href="#" data-align="right">
                                            <i class="fa fa-align-right" title="Align Right"></i>
                                        </a>
                                    </div>
                                </label>
                                <label for="style">Style
                                    <br>
                                    <div class="btn-group style">
                                        <a class="btn btn-default" href="#" data-style="bold">
                                            <i class="fa fa-bold" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-default" href="#" data-style="italic">
                                            <i class="fa fa-italic" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-default" href="#" data-style="underline">
                                            <i class="fa fa-underline" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </label>
                                <label for="color">Color
                                    <br>
                                    <div class="btn-group color">
                                        <a class="btn" href="#" style="background-color: black"></a>
                                        <a class="btn" href="#" style="background-color: #515151"></a>
                                        <a class="btn" href="#" style="background-color: #26a2e2"></a>
                                        <a class="btn" href="#" style="background-color: #05a11a"></a>
                                        <a class="btn" href="#" style="background-color: #e37804"></a>
                                    </div>
                                </label>
                            </div>

                            <!--     slick slider      -->

                            <div class="slider-holder">
                                <div id="category">
                                    <?php $args = array('taxonomy' => 'cards_category'); ?>
                                    <?php $categories = get_categories($args); ?>
                                    <div class="header-category">Category
                                        <p><span>1</span>/
                                            <?= count($categories) ?> Categories</p>
                                    </div>
                                    <div class="category-slider">
                                        <?php foreach ($categories as $category) : ?>
                                            <div>
                                                <p data-id="<?= $category->slug ?>">
                                                    <?= $category->name ?>
                                                </p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                                <div id="choose-text">
                                    <?php
                                    $query = new WP_Query(array(
                                        'posts_per_page' => -1,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'cards_category',
                                                'field' => 'slug',
                                                'terms' => $categories[0]->slug
                                            )
                                        )
                                    ));
                                    ?>
                                    <div class="header-choose-text">Choose text
                                        <p><span id="first-item">1</span>/<span
                                                id="count-posts"><?= count($query->posts) ?></span> Texts</p>
                                    </div>
                                    <div class="choose-text-slider">
                                        <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                                            <div>
                                                <p class="text">
                                                    <?= get_the_content(); ?>
                                                </p>
                                            </div>
                                        <?php endwhile; endif; ?>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="wrapper" style=" width: 980px; margin: auto; overflow: hidden;">
                        <div id="up-arrow" style="width:345px;float:left;    height: 70px;">
                            <label for="up">User the text above</label>

                            <div class="boder-input"></div>
                            <input id="up" type="button" value="" style="background:url('http://greetingletter.co.uk/wp-content/uploads/2016/08/up-arrow.png') no-repeat center;border:0px;border-style:none;    width: 40px;
    height: 40px;    margin-top: 15px;
    position: absolute; right: 20px;"/>
                        </div>
                        <div id="down-arrow" style="width:345px;float:right;    height: 70px;">
                            <label for="down">Personalize below</label>
                            <div class="boder-input"></div>
                            <input id="down" type="button" value="" style="background:transparent;border:0px;border-style:none;    width: 40px;
    height: 40px;    margin-top: 15px;
    position: absolute; left: 20px;"/>
                        </div>
                    </div>
                    <br>
                    <div class="textarea-wrapper">
              <textarea id="personal-text" class="text" disabled="disabled"
                        placeholder="Add your presonalized text here..."></textarea>
                    </div>
                    <div class="clearfix"></div>

                    <div class="page-footer">
                        <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/2by4.png" style=""/>
                        <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/scrolldown-2.png" style=""/>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="section2">
            <div class="intro">
                <div id="step3" style="width:100%;margin:0 auto;color: #141313 !important;display:block;">
                    <h3 style="text-align: center;margin-bottom: 100px;    font-family: lato;
    text-shadow: none;
    font-size: 30px;">Delivery Details</h3>
                    <div
                        style="margin:0 auto;width:689px;height:485px;background:url('http://greetingletter.co.uk/wp-content/uploads/2016/08/delivery-step.png') no-repeat;position: relative;">
                        <div id="delivery-options">
                            <input type="button"
                                   value="UK 1st Class letters                                            53p">
                            <input type="button"
                                   value="UK 2nd Class letters                                            40p">
                            <input type="button"
                                   value="International Letters                                           90p">
                        </div>
                        <div id="address-options">
                            <input type="text" placeholder="Street Name">
                            <input type="text" placeholder="House name or number">
                            <input type="text" placeholder="City">
                            <input type="text" placeholder="Postal Code">
                            <input type="text" placeholder="Region">
                            <input type="text" placeholder="Country">
                        </div>
                    </div>
                    <div class="page-footer">
                        <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/3by4.png"/>
                        <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/scrolldown-2.png"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" id="section3">
            <div class="intro">
                <div id="step4" style="width:100%;margin:0 auto;color: #141313 !important;-display:none; padding-bottom: 40px;">
                    <h3 style="text-align: center;margin-bottom: 100px;    font-family: lato;
    text-shadow: none;
    font-size: 30px;">Preview and Order</h3>

                    <div
                        style="margin: 0 auto;width:941px;height:576px;background:url('<?php echo get_stylesheet_directory_uri(); ?>/images/preview-order.png') no-repeat;">

                        <div style="width:100%;height: 338px;"><img id="sra3" src="<?php echo $imageselected; ?> "
                                                                    style="    margin-left: 80px;
    width: 312px;
    float: left;"/>
                            <img id="cardheightimage"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/images/small-height.png" style="    position: absolute;
    margin-left: -254px;
    margin-top: 90px;"/>

                            <img id="cardwidthimage"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/images/small-width.png" style="    position: absolute;
    margin-left: -478px;
    margin-top: 234px;"/>

                        </div>

                        <div id="chosen-text">
                            I love how I feel when I see your smile. I love that you laugh at all my silly jokes. I love
                            how you know me better than anyone and still love me. I love waking up next to you. Most of
                            all, I love that you said yes when I asked you to marry me. Happy anniversary, baby.
                        </div>
                        <div id="delivery-details">
                            UK 1st Class letters 53p
                        </div>
                        <div id="address-chosen">
                            1 Infinite Loop Cupertino, CA 95014, USA
                        </div>
                        <div style="width:50%;float:left;">
                            <input type="button" value="" style="background:url('http://greetingletter.co.uk/wp-content/uploads/2016/08/follow-up-on.png');width: 311px;
    height: 71px;
    border: 0;"/>
                        </div>
                        <div style="width:50%;float:left;">

                            <form method="post" action="../customize-greeting-letter-submit/capture-form.php">
                                <input name="sizefinal" id="sizefinal" type="hidden" value="Customary Size"/>
                                <input name="chosen-text-final" id="chosen-text-final" type="hidden" value=""/>
                                <input name="delivery-details-final" id="delivery-details-final" type="hidden"
                                       value="UK 1st Class letters                                            53p"/>
                                <input name="address-final" id="address-final" type="hidden" value=""/>
                                <input name="image-final" id="image-final" type="hidden"
                                       value="<?php echo $imageselected; ?>"/>
                                <input id="checkout-button" type="submit" value="" style="background:url('http://greetingletter.co.uk/wp-content/uploads/2016/08/procced-checkout.png');width: 350px;
    height: 83px;
    border: 0;margin-top: -8px;"/>
                            </form>
                        </div>
                    </div>
                    <h2 id="greet-display"></h2>
                    <div id="printcontainer"></div>
                    <img src="http://greetingletter.co.uk/wp-content/uploads/2016/08/4by4.png"
                         style="float:left; margin-left: 45px; margin-top: -60px;"/>
                </div>

            </div>
        </div>
    </div>
    </div>
    <?php //wp_footer(); ?>


    <?php
}
get_footer();