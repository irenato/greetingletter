<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */




get_header(); ?>
<?php

$banner_image = '';
$big_banner_image = '';
$banner_caption_text = 'banner caption text';

$call_to_action_button_text = 'button text';
$call_to_action_url = 'button url';
?>



	    <!-- Header ends -->
	<?php if($big_banner_image != ''){ ?>
	<!--
    <div class="sportshomebanner">
    	<img src="<?php echo $big_banner_image; ?>" alt="">
        <div class="sptitle">
        	<h1><?php echo $banner_caption_text; ?></h1>
        </div>
				<?php if($call_to_action_button_text != ''){ ?>
			<div class="planlink">
				<a href="<?php echo $call_to_action_url;?>"><?php echo $call_to_action_button_text;?></a>
			</div>
		<?php } ?>
    </div>
	-->
	<?php } ?>
<div class="topbannerad container">
	<div class="row">
<div id="ad-slot">
 
</div>
<!--
	   <img src="<?php echo $banner_image; ?>" alt="">
-->
	</div>
</div>



	
    <div class="container maincontent">
	
	

		<?php
	    query_posts("post_type=post&showposts=100");
	   if(have_posts()):while(have_posts()):the_post(); ?>
		<?php

		//$posts = get_field('related_posts',$current_post_id);
		$posts = get_field('related_posts',$post->ID);
			if( $posts ):
			?>
					<?php
					$duplicate_post = array();
					$i=0;
					foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
					<?php setup_postdata($post);
						$duplicate_post[$i] = $post->ID;
					?>
					<?php $i++; endforeach; ?>
				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
	  <?php  endwhile; endif; ?>



		<?php
		$pageNavi = (isset($_GET['pageNavi']) && $_GET['pageNavi'] != '') ? $_GET['pageNavi'] : 1;
		$next_pageNavi = $pageNavi+1;
		query_posts("post_type=post&paged=".$pageNavi);?>
        <div class="row gridbox" id="container">
			<?php if(have_posts()):while(have_posts()):the_post();
					$grid_size = get_field('grid_size');
					$lightbox_enable = get_field('lightbox_enable');

					$grid_class = ($grid_size == 'Big') ? 'col-sm-9 gridpostbig gridpost' : 'col-sm-3 gridpost';
					$grid_image_class = ($grid_size == 'Big') ? 'gridpicbig' : 'gridpic';
					$current_post_id = $post->ID;
		/*	if (!in_array($post->ID, $duplicate_post)){

			if ( is_array($duplicate_post) && in_array($post->ID, $duplicate_post) ) { */

			?>
        	<div class="<?php echo $grid_class;?>" id="<?php echo $lightbox_enable;?>">
				<?php
				$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full') );
				if ( has_post_thumbnail() ) { ?>
            	<div class="<?php echo $grid_image_class;?>">
					<?php
						//$format = get_post_format( $post_id );
						$format = get_post_format( $post->ID );
						$video_url =  get_field('video_url');
					?>
					<?php if($format == 'video'){ ?>
						<?php if($grid_size == 'Small'){ ?>
							<!--<a href="<?php echo $video_url;?>" class="fancybox-media"><img src="<?php echo $image;?>" alt=""></a>-->

							<?php

							if($video_url == '' && trim(get_post_meta( $post->ID, 'Player_embed_url', true )) == '')
								{
                  //$video_url = the_post_thumbnail('full');
									//$video_url1 = get_the_post_thumbnail($post->ID, 'full');
                  $video_shortcode = get_post_meta($post->ID, '_ustudio_featured_video_shortcode', true);
			            ?> <!-- <?php echo $video_shortcode; ?> --> <?php
                  $video_url1 = do_shortcode($video_shortcode);
			            ?> <!-- <?php echo $video_url1; ?> --> <?php
			            ?> <!-- <?php echo $post->ID; ?> --> <?php

$doc = new DOMDocument();
$doc->loadHTML($video_url1);

$src = $doc->getElementsByTagName('iframe')->item(0)->getAttribute('src');

$video_url = $src;



								}



							?>


							<div id="<?php echo basename($video_url);?>">

							<input id="playbutton-ustudio" class="play-video" type="image" src="<?php echo get_template_directory_uri()?>/page-templates/ustudioplay.png" value="<?php echo $video_url;?>" />
							<?php
							if($image == '')
							{
							echo '<img src="'.get_post_meta( $post->ID, 'Image_url', true ).'" alt="Greeting Letters"/>';
							}
							else
							{
							echo '<img src="'.$image.'" alt="Greeting Letters"/>';
							}

							?>

							</div>




						<?php }else{ ?>
							<?php

							echo do_shortcode('[youtube  width="638" height="418"]'.$video_url.'[/youtube]');



							?>
						<?php } ?>
					<?php }else{ ?>
						<?PHP if($lightbox_enable != 'YES'){ ?>
								<a href="#" class="greeting-back"><img src="<?php echo $image;?>" alt=""></a>
							<?PHP }else{ ?>
								<a href="<?php echo $image;?>" class="fancybox-media"><img src="<?php echo $image;?>" alt=""></a>




							<?php } ?>
					<?php } ?>
                </div>
				<?php }
				?>



            </div>
			<!-- related posts sections comes here ---------->
			<!--
					<?php
					$posts = get_field('related_posts',$current_post_id);
					if( $posts ):
					?>
					<div class="col-sm-3 gridpost">


							<?php
							$i=0;
							foreach( $posts as $post):  ?>
							<?php setup_postdata($post);
							?>

								<div class="gridspinfo innergridspinfo">
								<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
								<?php the_excerpt();?>
								<span class="related"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></span>
								</div>
							<?php $i++;
							endforeach; ?>



						<div class="clear"></div>
					</div>-->
						<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>
				<!-- related posts sections comes here ---------->


			<?php /*} */
			endwhile; endif; ?>
        </div>
		
		
        </div>
		
		
		
		
		
    </div>

			
			




			<div class="endlessscroll container">
				<div class="row">
					<h5></h5>
				</div>
			</div>
			<nav id="page_nav">
			  <a href="<?php echo home_url('')."/?pageNavi=".$next_pageNavi;?>"></a>
			</nav>
        <div class="container">
        <div class="row">	
			<div class="footer">
				<h6>POWERED BY <h5>Greeting Letter</h5></h6>
				<!--<div class="footercomlogo"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/webme.png" alt=""></a></div>-->
				<div class="copyrightwrapper">
					<div class="copyrightbar">
							<?php
									if(function_exists('ot_get_option')){
										$footer_section = ot_get_option('footer_section');
										echo $footer_section;
									}
									?>
						<div class="clear"></div>
					</div>
				</div>
			 </div>		
		</div>		
	</div>	



<style>
@media (max-width:700px){


.popup-content {
  top: 10%;
  left: 0% !important;
   width: 100% !important;
}}
</style>


<div class="popup-content">
<div class="popup-content-video">

</div>
</div>


<div id="popup" style="  background: #000000;  opacity: 0.5;  position: fixed;  width: 100%;  height: 2500px;  z-index: 1000;  top: 0px;
  left: 0px; display:none;">

    <div class="transparency">

    </div>


</div>




<?php
get_footer();
