<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();



			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			get_template_part( 'content', get_post_format() );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			
		// End the loop.
		endwhile;
		?>



<?php //query_posts('post_type=greeting_patterns&post_status=publish&posts_per_page=100&paged='. get_query_var('paged')); ?>
					<?php if( have_posts() ): ?>
					
                   
						<?php while( have_posts() ): the_post(); 
						$pattern_image= get_field('pattern_image');
?>
							
<style>
body{
background:url(<?php echo $pattern_image; ?>) repeat !important;
}
</style>
						<?php endwhile; ?>
                   
					<?php endif; wp_reset_query(); ?>


		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
