<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if IE 7]>
<html class="ie ie7">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html>
<!--<![endif]-->

<head>

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<!-- SET: STYLESHEET -->
	<link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css"> 
	<?php wp_head(); ?>
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.selectbox.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.fancybox-buttons.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/print.css" rel="stylesheet" type="text/css" media="print">
	<!-- END: STYLESHEET -->

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css" rel="stylesheet" />
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/perspectiveRules.css" rel="stylesheet" />
	
	
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/flat-ui.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/main.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/photopile.css" rel="stylesheet">

	
	<link href="https://www.exinary.com/wp-content/themes/exinaryV1/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
      <![if !(lte IE 8)]>
      <link rel="apple-touch-icon" href="https://www.exinary.com/wp-content/themes/exinaryV1/favicon.ico">
      <link rel="apple-touch-icon" sizes="76x76" href="https://www.exinary.com/wp-content/themes/exinaryV1/favicon.ico">
      <link rel="apple-touch-icon" sizes="120x120" href="https://www.exinary.com/wp-content/themes/exinaryV1/favicon.ico">
      <link rel="apple-touch-icon" sizes="152x152" href="https://www.exinary.com/wp-content/themes/exinaryV1/favicon.ico">

        <meta name="mobile-web-app-capable" content="yes">
        <link rel="icon" sizes="192x192" href="https://www.exinary.com/wp-content/themes/exinaryV1/favicon.ico">

<meta name="description" content="" />
<meta name="keywords" content="" />


</head>


	<body>



   