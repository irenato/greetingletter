<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
			 <div id="footers"></div>
		</div>
	<!-- wrapper ends -->
	
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.dragscroll.js"></script>
	
	

	<?php wp_footer(); ?>
    


</body>
</html>