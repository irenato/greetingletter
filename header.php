<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if IE 7]>
<html class="ie ie7">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8">
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html>
<!--<![endif]-->

<head>

    <title><?php wp_title('|', true, 'right'); ?></title>

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico"/>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <!-- SET: STYLESHEET -->
    <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css">
    <?php wp_head(); ?>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.selectbox.css" rel="stylesheet"
          type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/jquery.fancybox-buttons.css" rel="stylesheet"
          type="text/css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/print.css" rel="stylesheet" type="text/css"
          media="print">
    <!-- END: STYLESHEET -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css" rel="stylesheet"/>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/perspectiveRules.css" rel="stylesheet"/>


    <link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/flat-ui.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/main.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/photopile.css" rel="stylesheet">


    <link href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon.ico" rel="shortcut icon"
          type="image/vnd.microsoft.icon"/>
    <![if !(lte IE 8)]>
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="76x76"
          href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="120x120"
          href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152"
          href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/apple-icon-152x152.png">

    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192"
          href="<?php echo get_stylesheet_directory_uri(); ?>/favicon/android-icon-192x192.png">
    <link rel="stylesheet" type="text/css" href="../wp-content/themes/greeting-letter/production/jquery.fullPage.css"/>
    <link rel="stylesheet" type="text/css"
          href="../wp-content/themes/greeting-letter/production/fullpagescroll/examples.css"/>

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Cantarell"
          class="changeFontUP">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Cantarell"
          class="changeFontDown">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../wp-content/themes/greeting-letter/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/style.css"/>

    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/nav.js"></script>


    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/jquery-ui.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap-select.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/bootstrap-switch.js"></script>

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/photopile_files/photopile.js"></script>


    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.particleground.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/snap-svg.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/jquery.logosDistort.js"></script>

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/steps.js"></script>


    <!--[if IE]>
    <script type="text/javascript">
        var console = {
            log: function () {
            }
        };
    </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script type="text/javascript" src="../wp-content/themes/greeting-letter/production/jquery.fullPage.js"></script>
    <script type="text/javascript"
            src="../wp-content/themes/greeting-letter/production/fullpagescroll/examples.js"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('#fullpage').fullpage({
                sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'],
                anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
                menu: '#menu',
                navigation: true,
                navigationPosition: 'right',
                navigationTooltips: ['Step One', 'Step Two', 'Step Three', 'Step Four'],


                easingcss3: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)'
            });


        });
    </script>


</head>


<body>



   